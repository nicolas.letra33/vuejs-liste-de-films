import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false

window.movies_list = {
  movies: [
    {
      id: 1,
      title: "Fight Club",
      img: "https://m.media-amazon.com/images/I/71zEIB3ekZL._AC_SY606_.jpg",
      year: 1999,
      language: "américain",
      realinfo: {
        name: "David Fincher",
        nationality:"Américain",
        birthdate:"28/08/1962"
      },
      type: "psychologique",
      grade: null
    },
    {
      id: 2,
      title: "Reservoir Dogs",
      img: "https://fr.web.img6.acsta.net/medias/nmedia/18/35/49/60/18869530.jpg",
      year: 1992,
      language: "américain",
      realinfo: {
        name:"Quentin Tarantino",
        nationality:"Américain",
        birthdate:"27/03/1963"
      },
      type: "gangster",
      grade: null
    },
    {
      id: 3,
      title: "Sixieme Sens",
      img: "https://fr.web.img6.acsta.net/medias/nmedia/18/66/15/77/19255607.jpg",
      year: 1999,
      language: "américain",
      realinfo: {
        name:"M. Night Shyamalan",
        nationality:"Américain",
        birthdate:"06/08/1970"
      },
      type: "thriller",
      grade: null
    },
    {
      id: 4,
      title: "Alien",
      img: "https://images.photowall.com/products/59754/alien.jpg?h=699&q=85",
      year: 1979,
      language: "américain",
      realinfo: {
        name:"Ridley Scott",
        nationality:"Anglais",
        birthdate:"30/10/1937"
      },
      type: "horreur - science fiction",
      grade: null
    }
  ]
}

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')

