# VueJS - Liste de film

## Présentation

Ce petit projet VueJS a été développé dans le cadre d'un apprentissage du framework.  

L'objectif étant de manipuler et d'afficher des données en utilisant l'architecture de VueJS.  

Ici il est question d'une liste de film qui s'affiche sur la page d'accueil, il y a la possibilité de filtrer les résulats via une barre de recherche, d'ajouter un film, d'en supprimer, d'accéder à la page détaillée des informations d'un film et d'en éditer le contenu.  

Les données des films sont stockées en dur dans un tableau d'objets, étant donné que le but était de se concentrer sur VueJS et la gestion du front.

## Utilisation du projet

### Setup du projet
```
npm install
```

### Lancement du serveur de dev
```
npm run serve
```

### Compilation pour prod
```
npm run build
```